from django.db import models

class User(models.Model):

  username = models.CharField(max_length=254, unique=True, db_index=True)
  first_name = models.CharField(max_length=254)
  last_name = models.CharField(max_length=254)
  email = models.EmailField(max_length=254, unique=True)
  gallery = models.ForeignKey('Gallery')
  slug = models.SlugField(unique=True)
  discription = models.TextField(blank=True)

  def __str__(self):
    return ' '.join([
      self.first_name,
      self.last_name,
    ])

class Gallery(models.Model):

  name = models.CharField(max_length=254)
  slug = models.SlugField(unique=True)
  discription = models.TextField(blank=True)

  def __unicode__(self):
    return self.name



