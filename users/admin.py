from django.contrib import admin
from users.models import User, Gallery

class UserAdmin(admin.ModelAdmin):

  prepopulated_fields = {'slug': ('username',)}
  list_display = ('username', 'gallery')
  search_fields = ['name']

class GalleryAdmin(admin.ModelAdmin):

  prepopulated_fields = {'slug': ('name',)}


admin.site.register(User, UserAdmin)
admin.site.register(Gallery, GalleryAdmin)
